\section{Git internals}

\subsection{Git terminology}
\frame{%
    \frametitle{Git terminology}
    \begin{center}
        \begin{tikzpicture}
            \tikzstyle{remote}=[ellipse, draw, fill=red!33]
            \tikzstyle{repo}=[ellipse, draw, fill=black!33]
            \tikzstyle{stage}=[ellipse, draw, fill=green!33]
            \tikzstyle{wd}=[ellipse, draw, fill=blue!33]
            \tikzset{every node/.style={align=center}}

            \draw[dashed, opacity=0.5] (-5,0.8) -- (5,0.8);
            \node[below right, opacity=0.5] at (-5,0.8) {\scriptsize local};

            \draw (0, 2.50) node[remote]    (re)    {remote};
            \draw (0, 0.00) node[repo]      (ry)    {repository};
            \draw (0,-1.25) node[stage]     (se)    {stage / index};
            \draw (0,-2.50) node[wd]        (wy)    {working directory};

            \draw<2>  [thick,->]        (re) to [bend right=70]                                   (ry);
            \draw<2>  [thick,->]        (re) to [bend right=70]                                   (se);
            \draw<2>  [thick,->]        (re) to [bend right=70] node[left]  {clone, pull}         (wy);

            \draw<3-7>[thick,dashed,->] (ry) to [bend right=70]                                   (se);
            \draw<3-7>[thick,->]        (ry) to [bend right=75] node[left]  {checkout, reset\\
                                                                             merge, rebase,\\
                                                                             cherry-pick,\\
                                                                             revert}              (wy);
            \draw<3-7>[thick,dashed,->] (se) to [bend right=70]                                   (wy);


            \draw<4-> [thick,->]        (wy) to [bend right=70] node[right] {stage / add, mv, rm} (se);

            \draw<5-> [thick,->]        (se) to [bend right=70] node[right] {commit}              (ry);

            \draw<6-> [thick,->]        (ry) to [bend right=70] node[right] {push}                (re);

            \draw<7>  [thick,->]        (re) to [bend right=70] node[left]  {fetch}               (ry);
        \end{tikzpicture}
    \end{center}
}



\subsection{The repository}
\frame{%
    \frametitle{The repository}
    \begin{itemize}
        \item All data is stored under a single directory called \texttt{.git} in the top-level directory of your project.
        \item Git tracks \emph{content}, not differences or files as such.
        \item A commit is essentially a checksummed, annotated snapshot of all (relevant) data in a working directory.
        \item The heart of the repository is a directed acyclic graph of cryptographic checksums (\textit{Merkle-DAG}) over the contents of files and directories.
        \item On each and every access, all data read from the repository is checksummed again and thus verified.
        \item Submodules are \emph{fully-fledged} repositories referenced by and embedded into a ``superproject''.
    \end{itemize}
}



\subsection{SHA-1}
\frame{%
    \frametitle{SHA-1}
    \begin{itemize}
        \item \emph{Secure Hash Algorithm} is a 160 bit cryptographic hash function:
            \begin{itemize}
                \item Arbitrary input $\to$ 160 bit checksum (\emph{hash}).
                \item Flipping a single bit anywhere in the input data leads to a substantially different different checksum.
                \item It is next to impossible to manipulate data and still get the same checksum.
            \end{itemize}
        \item Every object is identified and referenced by its SHA-1 hash.
        \item Every time Git accesses an object, it validates the hash.
        \item If you change only a single character in a single file, all hashes up to the commit change!
    \end{itemize}
}



\subsection{Objects}
\frame{%
    \frametitle{Objects}
    \begin{itemize}
        \item There are four elementary object types in Git:
        \begin{itemize}
            \item blob $\leadsto$ a file.
            \item tree $\leadsto$ a directory.
            \item commit $\leadsto$ a particular state of the working directory.
            \item tag $\leadsto$ an annotated tag (we will ignore those for now).
        \end{itemize}
    \end{itemize}

    \begin{center}
        \begin{tikzpicture}
            \tikzstyle{blob}=[rectangle, draw, rounded corners, fill=red!33]
            \tikzstyle{tree}=[rectangle, draw, rounded corners, fill=blue!33]
            \tikzstyle{commit}=[rectangle, draw, rounded corners, fill=green!33]

            \draw[opacity=0] (-2,0) -- (6,0);

            \draw<1-> (-1,2)    node[left]      (c0)    {\dots};

            \draw<1-> (0.5,2)   node[commit]    (c1)    {commit};
            \draw<2-> (3.5,2)   node[commit]    (c2)    {commit};

            \draw<1-> (0.5,1)   node[tree]      (t1)    {tree};
            \draw<2-> (3.5,1)   node[tree]      (t2)    {tree};

            \draw<1-> (0,0)     node[blob]      (b1)    {blob};
            \draw<1-> (2,0)     node[tree]      (t3)    {tree};
            \draw<1-> (1,-1)    node[blob]      (b2)    {blob};
            \draw<1-> (3,-1)    node[blob]      (b3)    {blob};
            \draw<2-> (4,0)     node[blob]      (b4)    {blob};

            \draw<1->[thick,<-] (b1) -- (t1);
            \draw<1->[thick,<-] (t1) -- (c1);
            \draw<1->[thick,<-] (t3) -- (t1);

            \draw<1->[thick,<-] (b2) -- (t3);
            \draw<1->[thick,<-] (b3) -- (t3);

            \draw<2->[thick,<-] (t3) -- (t2);
            \draw<2->[thick,<-] (b4) -- (t2);
            \draw<2->[thick,<-] (t2) -- (c2);

            \draw<1->[thick,<-] (c0) -- (c1);
            \draw<2->[thick,<-] (c1) -- (c2);
        \end{tikzpicture}
    \end{center}
}

\frame{%
    \frametitle{Objects -- detailed}
    \begin{itemize}
        \item A blob is simply the content of a particular file plus some meta-data.
        \item A tree is a plain text file, which contains a list of blobs and/or trees with their corresponding file modes and names.
        \item A commit is also a plain text file containing information about the author of the commit, a timestamp and references to the parent commit(s) and the corresponding tree.
        \item All objects are compressed with the \texttt{DEFLATE} algorithm and stored in the Git object database under \texttt{.git/objects}.
        \item When the number of files exceeds a certain limit, objects are glued together in a ``pack'' for performance reasons.
    \end{itemize}
}

\frame{%
    \frametitle{Combining hashes and objects}
    \begin{center}
        \begin{tikzpicture}
            \draw[opacity=0] (-1.5,0) -- (8.5,0);

            \tikzstyle{blob}=[rectangle split, rectangle split parts=2, draw, rounded corners, fill=red!33]
            \tikzstyle{tree}=[rectangle split, rectangle split parts=2, draw, rounded corners, fill=blue!33]
            \tikzstyle{commit}=[rectangle split, rectangle split parts=2, draw, rounded corners, fill=green!33]

            \draw<1-> (-0.5,4)  node[left]      (c0)    {\dots};
            \draw<1-> (1,4)     node[commit]    (c1)    {commit \nodepart{two} 9967ccb};
            \draw<2-> (6,4)     node[commit]    (c2)    {commit \nodepart{two} 57be356};

            \draw<1-> (1,2)     node[tree]      (t1)    {tree \nodepart{two} a910656};
            \draw<2-> (6,2)     node[tree]      (t2)    {tree \nodepart{two} 41fe540};

            \draw<1-> (0,0)     node[blob]      (b1)    {blob \nodepart{two} 80146af};
            \draw<1-> (2,0)     node[blob]      (b2)    {blob \nodepart{two} 50fcd26};
            \draw<2-> (7,0)     node[blob]      (b3)    {blob \nodepart{two} d542920};

            \draw<1->[thick,<-] (b1) -- (t1);
            \draw<1->[thick,<-] (b2) -- (t1);
            \draw<1->[thick,<-] (t1) -- (c1);

            \draw<2->[thick,<-] (b3) -- (t2);
            \draw<2->[thick,<-] (b2) -- (t2);
            \draw<2->[thick,<-] (t2) -- (c2);

            \draw<1->[thick,<-] (c0) -- (c1);
            \draw<2->[thick,<-] (c1) -- (c2);
        \end{tikzpicture}
    \end{center}
}

\frame{%
    \frametitle{Down the rabbit hole\dots}
    There is a so-called \emph{plumbing} command that lets you inspect the complete Merkle-DAG node by node:
    \shell{\footnotesize%
        \$ git cat-file -t b255475 \newline
        commit \newline
        \$ git cat-file -p b255475 \newline
        tree 719eb559e2ef8d2f9d21df6853dd120899315380 \newline
        parent 59b8b022a42d7726de50258aec1c49d8c219ea86 \newline
        author Sebastian Neuser <haggl@sineband.de> 1480547766 +0100 \newline
        committer Sebastian Neuser <haggl@sineband.de> 1480547766 +0100 \newline
        \newline
        Change default output directory to "generated\_files" \newline
        \$ git cat-file -p 719eb559e2ef8d2f9d21df6853dd120899315380 \newline
        100644 blob 419e52be98bc4c487c652e43df3fe6c60195f30a    README \newline
        100755 blob 0fb49084a157fb237a2355c0aadcc176c569d896    generate-commits
    }
}



\subsection{References}
\frame{%
    \frametitle{References}
    \begin{itemize}
        \item \master\ (as well as every other ref) is a text file which contains nothing more than the hash of the latest commit made on the branch:
              \shell{%
                  \$ cat~.git/refs/heads/master \newline
                  57be35615e5782705321e5025577828a0ebed13d
              }
        \item Lightweight \gittag{tags} are basically the same as branches, except they are not updated automatically when you add a commit.
        \item \head\ is also a text file and contains a pointer to the last reference (or commit) that was checked out:
              \shell{%
                  \$ cat~.git/HEAD \newline
                  ref: refs/heads/master
              }
    \end{itemize}
}

\frame{%
    \frametitle{The Merkle DAG}
    \begin{center}
        \begin{tikzpicture}
            \tikzstyle{blob}=[rectangle split, rectangle split parts=2, draw, rounded corners, fill=red!33]
            \tikzstyle{tree}=[rectangle split, rectangle split parts=2, draw, rounded corners, fill=blue!33]
            \tikzstyle{commit}=[rectangle split, rectangle split parts=2, draw, rounded corners, fill=green!33]
            \tikzstyle{ref}=[rectangle, draw, rounded corners]

            \draw (-1,3)    node            (past)  {\dots};
            \draw (1,3)     node[commit]    (c1)    {commit \nodepart{two} 9967ccb};
            \draw (6,3)     node[commit]    (c2)    {commit \nodepart{two} 57be356};

            \draw (1,1.5)   node[tree]      (t1)    {tree \nodepart{two} a910656};
            \draw (6,1.5)   node[tree]      (t2)    {tree \nodepart{two} 41fe540};

            \draw (0,0)     node[blob]      (b1)    {blob \nodepart{two} 80146af};
            \draw (2,0)     node[blob]      (b2)    {blob \nodepart{two} 50fcd26};
            \draw (7,0)     node[blob]      (b3)    {blob \nodepart{two} d542920};

            \draw (1,4.5)   node[ref]       (tag)   {\gittag{v4.2}};
            \draw (6,4.5)   node[ref]       (mstr)  {\master};
            \draw (6,5.5)   node[ref]       (head)  {\head};

            \draw[thick,<-] (b1) -- (t1);
            \draw[thick,<-] (b2) -- (t1);
            \draw[thick,<-] (t1) -- (c1);

            \draw[thick,<-] (b3) -- (t2);
            \draw[thick,<-] (b2) -- (t2);
            \draw[thick,<-] (t2) -- (c2);

            \draw[thick,<-] (past) -- (c1);
            \draw[thick,<-] (c1) -- (c2);

            \draw[thick,<-] (c1) -- (tag);

            \draw[thick,<-] (mstr) -- (head);
            \draw[thick,<-] (c2) -- (mstr);
        \end{tikzpicture}
    \end{center}
}



\subsection{The staging area}
\frame{%
    \frametitle{The staging area}
    \begin{itemize}
        \item The staging area is kind of an intermediate layer between the working directory and the repository, also called \emph{index}.
        \item It is a binary file (\texttt{.git/index}) containing a virtual working tree state used to compose commits:
            \begin{itemize}
                \item Changes can be marked to go into the next commit one by one.
                \item Changes can be reverted and overridden.
                \item Git can show differences between stage, working directory and repository.
            \end{itemize}
        \item When committing,
            \begin{itemize}
                \item the virtual tree of the staging area is tagged with meta data (author name, timestamp, \dots),
                \item the last commit is referenced as \emph{parent},
                \item the currently checked out branch (or \head) is updated to point to the new commit.
            \end{itemize}
    \end{itemize}
}

\frame{%
    \frametitle{The staging area -- Shell example}
    The Git plumbing command \texttt{git ls-files} parses the index file and shows its contents:
    \shell{\footnotesize%
        \$ git ls-files -{}-stage \newline
        100644 d542920062e17fd9e20b3b85fd2d0733b1fb7e77 0 \ \ \ \ \  file0 \newline
        100644 50fcd26d6ce3000f9d5f12904e80eccdc5685dd1 0 \ \ \ \ \  file1 \newline
        \$ echo File 2 > file2 \newline
        \$ git add file2 \newline
        \$ git ls-files -{}-stage \newline
        100644 d542920062e17fd9e20b3b85fd2d0733b1fb7e77 0 \ \ \ \ \  file0 \newline
        100644 50fcd26d6ce3000f9d5f12904e80eccdc5685dd1 0 \ \ \ \ \  file1 \newline
        100644 4475433e279a71203927cbe80125208a3b5db560 0 \ \ \ \ \  file2
    }
}
