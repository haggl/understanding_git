\section{Using Git}

\subsection{Creating or cloning a repository}
\frame{%
    \frametitle{Creating or cloning a repository -- Creating}
    \begin{itemize}
        \item Initializing a repository:
              \shell{\scriptsize%
                \$ git init demo \newline
                Initialized empty Git repository in /home/haggl/demo/.git/
              }
              This command automatically creates a working directory and within it a repository ($\leadsto$ the \texttt{.git}-directory) with a branch called \master, which is checked out.
        \item Initializing a bare repository (for server-side operation):
              \shell{\scriptsize%
                \$ git init -{}-bare bare.git \newline
                Initialized empty Git repository in /home/haggl/bare.git/
              }
              This command creates only the repository itself and the \master-branch.
    \end{itemize}
}

\frame{%
    \frametitle{Creating or cloning a repository -- Cloning}
    \shell{\footnotesize%
        \$ git clone git@gitlab.com:haggl/dotfiles.git \newline
        Cloning into dotfiles\dots \newline
        remote: Counting objects: 488, done. \newline
        remote: Compressing objects: 100\% (251/251), done. \newline
        remote: Total 488 (delta 249), reused 434 (delta 202) \newline
        Receiving objects: 100\% (488/488), 86.36 KiB, done. \newline
        Resolving deltas: 100\% (249/249), done.
    }
    \svsp
    This command creates a working directory, makes a \emph{full copy} of the original repository and checks out the \master-branch.
    It also sets up the remote repository you cloned from and names it \origin.
}



\subsection{Staging and committing files}
\frame{%
    \frametitle{Staging and committing files -- Staging}
    \begin{itemize}
        \item Staging a new or changed file or folder:
              \shell{\$ git stage <path>}
        \item Staging parts of a changed file or folder:
              \shell{\$ git stage -p <path>}
        \item Staging all changed files (not adding new ones!):
              \shell{\$ git stage -u}
        \item Removing changes to a file from the index ($\leadsto$ un-staging it):
              \shell{\$ git restore --staged <path>}
    \end{itemize}
}

\frame{%
    \frametitle{Adding and committing files -- Manipulating files}
    \begin{itemize}
        \item Discarding all changes to a file or directory:
              \shell{\$ git restore <path>}
        \item Renaming a file or directory:
              \shell{\$ git mv <old name> <new name>}
        \item Removing a file:
              \shell{\$ git rm <file>}
    \end{itemize}
}

\frame{%
    \frametitle{Adding and committing files -- Committing}
    \begin{itemize}
        \item Simple commit, directly specifying the message:
              \shell{\$ git commit -m'<message>'}
        \item Overwriting the last commit ($\leadsto$ \head) in case you messed up or forgot something:
              \shell{\$ git commit -{}-amend}
        \item Committing all changes (but not new files!):
              \shell{\$ git commit -a}
    \end{itemize}
}



\subsection{Displaying information}
\frame{%
    \frametitle{Displaying information -- Index and working directory}
    \begin{itemize}
        \item Showing a summary of the working directory and the index:
              \shell{\$ git status}
        \item Showing a \texttt{diff} between the working directory and the index:
              \shell{\$ git diff}
        \item Showing a \texttt{diff} between the index and \head:
              \shell{\$ git diff -{}-cached}
    \end{itemize}
}

\frame{%
    \frametitle{Displaying information -- Index and working directory}
    \begin{itemize}
        \item Showing the \texttt{diff} of a specific commit:
              \shell{\$ git show <commit>}
        \item Showing the commit log of the current branch:
              \shell{\$ git log}
        \item Commit log with a summary of each commit's changed files:
              \shell{\$ git log -{}-stat}
        \item Drawing a nice graph of all commits in all branches:
              \shell{\$ git log -{}-graph -{}-oneline -{}-decorate -{}-all}
    \end{itemize}
}



\subsection{Handling branches}
\frame{%
    \frametitle{Handling branches}
    \begin{itemize}
        \item Creating a branch pointing to \head:
              \shell{\$ git branch <branch name>}
        \item Creating a branch pointing to a specific commit:
              \shell{\$ git branch <branch name> <commit>}
        \item Checking out a branch:
              \shell{\$ git switch <branch name>}
        \item Deleting a branch:
              \shell{\$ git branch -D <branch name>}
    \end{itemize}
}

\frame{%
    \frametitle{Handling branches -- continued}
    \begin{itemize}
        \item Renaming a branch:
              \shell{\$ git branch -m <old name> <new name>}
        \item Letting a branch point to a specific commit:
              \shell{\$ git branch -f <branch name> <commit>}
        \item Merging a branch into \head:
              \shell{\$ git merge <branch name>}
    \end{itemize}
}



\subsection{Stashing}
\frame{%
    \frametitle{Stashing}
    \begin{itemize}
        \item Saving the current working directory and index state into commits and revert the changes to match \head:
              \shell{\$ git stash}
        \item Listing all available stashes:
              \shell{\$ git stash list}
        \item Showing the \texttt{diff} of a particular stash:
              \shell{\$ git stash show <stash>}
        \item Applying and deleting a particular stash:
              \shell{\$ git stash pop <stash>}
    \end{itemize}
}



\subsection{Working with remote repositories}
\frame{%
    \frametitle{Working with remote repositories -- Management}
    \begin{itemize}
        \item Adding a remote repository:
              \shell{\$ git remote add <name> <url>}
        \item Renaming a remote repository:
              \shell{\$ git remote rename <old name> <new name>}
        \item Removing a remote repository:
              \shell{\$ git remote rm <name>}
    \end{itemize}
}

\frame{%
    \frametitle{Working with remote repositories -- Downstream data flow}
    \begin{itemize}
        \item Updating all refs from all remote repositories, downloading the necessary objects and deleting obsolete remote refs:
              \shell{\$ git fetch -{}-all -{}-prune}
        \item Updating all refs from a specific remote repository and downloading necessary objects:
              \shell{\$ git fetch <remote>}
        \item Downloading objects and refs from a remote repository and merging a branch into \head:
              \shell{\$ git pull <remote> <branch>}
    \end{itemize}
}

\frame{%
    \frametitle{Working with remote repositories -- Upstream data flow}
    \begin{itemize}
        \item Pushing a branch to a remote repository:
              \shell{\$ git push <remote> <branch>}
        \item Deleting a branch from a remote repository:
              \shell{\$ git push <remote> -{}-delete <branch>}
        \item Overwriting a branch on a remote repository:
              \shell{\$ git push -f <remote> <branch>}
              \textbf{\textcolor{red}{WARNING:}} When you are collaborating with others you should not use this command, because it messes up their repositories due to changing hashes!
    \end{itemize}
}
