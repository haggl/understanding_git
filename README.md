# Understanding Git - How *not* to lose your `HEAD`
An instructional presentation about Git SCM and setup scripts for common scenarios.


## Presentation
1. Introduction
    * Who am I?
    * Terms and conditions
    * What is Git?
    * What is Git not?
2. Git internals
    * Object types
    * The SHA-1
    * References
    * The staging area
3. Using Git
    * Creating or cloning a repository
    * Staging and committing files
    * Displaying information
    * Handling branches
    * Stashing
    * Working with remote repositories
4. Collaboration
    * Commit guidelines
    * Topic branches
    * Rebasing
    * Pull/Merge requests
    * Typical workflow
5. Conclusion
    * Pros
    * Cons
    * Final thoughts
    * Further information


## Demos
The [demos](./demos) directory contains some simple shell scripts which set up the repositories used for the demonstrations during the presentation.


## Generators
The [workshop](./workshop) directory contains scripts that quickly and easily generate random commits and/or whole repositories for testing purposes.
You can use them to try things out and mess around with potentially dangerous commands.
Please refer to the submodule's README file for further information.


## License
<a rel="license" href="http://creativecommons.org/licenses/by-sa/3.0/deed.en_US"><img alt="Creative Commons License" style="border-width:0" src="http://i.creativecommons.org/l/by-sa/3.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">Understanding Git</span> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/3.0/deed.en_US">Creative Commons Attribution-ShareAlike 3.0 Unported License</a>.
