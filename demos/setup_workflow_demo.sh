#!/bin/sh
# This script sets up a Git repository containing two one-liner files
# and modifies (and commits) one of the files.
# @author Sebastian Neuser


# create a "remote" repository with some initial commits
git init workflow_demo_remote
cd workflow_demo_remote
for i in `seq 0 7`; do
    echo "This is the initial content of file #$i." > file$i
    git add .
    git commit -m "Add file number $i."
done


# clone the "remote" repository
cd ..
git clone workflow_demo_remote workflow_demo_local


# create further commits in the "remote" repository
cd workflow_demo_remote
for file in file[0-3]; do
    sed -i 's/initial/updated/' $file
    git stage $file
done
git commit -m 'Change contents of files 0 through 3.'

for file in file[4-7]; do
    sed -i 's/initial/updated/' $file
    git stage $file
done
git commit -m 'Change contents of files 4 through 7.'
