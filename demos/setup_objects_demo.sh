#!/bin/sh
# This script sets up a very simple Git repository containing two one-liner
# files, modifies one of them and renames the other.
# Intended for an interactive demo of the storage layer with `git cat-file`.
# @author Sebastian Neuser


git init objects_demo
cd objects_demo
git commit --allow-empty -m'Initialize empty repository'
echo File 0 > file0
echo File 1 > file1
git add .
git commit -m 'Add two simple files'
git tag 'v4.2'
sed -i 's/File 0/This is \0/' file0
git add file0
git commit -m 'Change contents of file0'
git mv file1 file_one
git commit -m 'Rename file1'
