#!/bin/sh
# This script sets up a Git repository containing several one-liner files,
# and creates two branches with changes to those.
# @author Sebastian Neuser


# create repository
git init rebase_demo
cd rebase_demo

# create initial commit
for i in `seq 0 9`; do
    echo "This is the initial content of file #$i." > file$i
done
git add .
git commit -m 'Initial commit: Add ten simple text files.'

# create two topic branches
git branch rebaseme
git branch mergeme

# create a commit on the rebase branch
git checkout rebaseme
for file in file[4-6]; do
    sed -i 's/initial/rebase branch/' $file
    git stage $file
done
git commit -m 'Change contents of files 4 through 6.'

# wait a second so that timestamps differ
sleep 1

# create a commit on the merge branch
git checkout mergeme
for file in file[7-9]; do
    sed -i 's/initial/merge branch/' $file
    git stage $file
done
git commit -m 'Change contents of files 7 through 9.'

# wait a second so that timestamps differ
sleep 1

# create second commit on the master branch
git checkout master
for file in file[0-3]; do
    sed -i 's/initial/updated/' $file
    git stage $file
done
git commit -m 'Change contents of files 0 through 3.'
