\section{Collaboration}

\subsection{Commit guidelines}
\frame{%
    \frametitle{Commit guidelines -- Do}
    \begin{itemize}
        \item Conventions regarding the commit message:
            \begin{enumerate}
                \item The first line is a short ($\leq$ 50 characters) description of what the commit is doing in imperative present tense.
                \item Second line is blank.
                \item Following lines describe the commit in more detail.
            \end{enumerate}
        \item Make small commits! The shorter your commit-\texttt{diff}s, the easier it will be to find and fix things later on.
        \item Commit early! Once you have committed a change, it's safe. You can \www{http://sethrobertson.github.io/GitBestPractices/\#sausage}{hide the sausage making} later (\emph{before} pushing, of course!) by amending the commit(s).
    \end{itemize}
}

\frame{%
    \frametitle{Commit guidelines -- Do \emph{not}}
    \begin{itemize}
        \item Don't make commits which leave the working directory in a broken state!
              Check if everything compiles cleanly before committing.
        \item Don't commit files that can be generated!
        \item Don't commit configuration files that have to be adjusted on a per-workstation basis!
        \item Don't commit large binaries unless it is absolutely necessary!
    \end{itemize}
}



\subsection{Topic branches}
\frame{%
    \frametitle{Topic branches}
    \begin{itemize}
        \item Aside from really small changes like cosmetics in comments, whitespace issues and so on you should normally never work on the \master-branch but create a special so-called \emph{topic-branch}.
        \item If you have an issue tracking system like the one on GitHub / in GitLab, it is reasonable to name the topic branches after the issue / ticket -- perhaps with a token that identifies you:
            \shell{\$ git branch sn/issue42 origin/master}
    \end{itemize}
}



\subsection{Rebasing}
\frame{%
    \frametitle{Rebasing}
    \begin{itemize}
        \item A rebase essentially does the following:
        \begin{enumerate}
            \item Save all commits that are in the current branch but not in the target branch as \texttt{diff}s.
            \item Rewind \head\ to the common ancestor of the two branches.
            \item Apply all commits from the target branch.
            \item Apply all saved \texttt{diff}s on the target branch in order.
        \end{enumerate}
        \item The resulting tree is exactly the same as if the two branches had been merged, only the commit history looks different.
        \item With an interactive rebase you can amend commits further down in the history, change commit messages and even rearrange commits. This is where the fun begins! ;-)
    \end{itemize}
}

\frame{%
    \frametitle{Rebasing -- visualized (starting point)}
    \begin{center}
        \begin{tikzpicture}
            \tikzstyle{commit}=[rectangle, draw, rounded corners, fill=green!33]
            \tikzstyle{ref}=[rectangle, draw, rounded corners]

            \draw (0,1)     node[commit]    (cm)    {dcc1a52};
            \draw (0,2)     node[ref]       (rm)    {\local{mergeme}};

            \draw (2,0)     node[commit]    (c1)    {0e74e93};
            \draw (2,1)     node[commit]    (c2)    {89f8555};
            \draw (2,2)     node[ref]       (mstr)  {\master};
            \draw (2,3)     node[ref]       (head)  {\head};

            \draw (4,1)     node[commit]    (cr)    {a5edb85};
            \draw (4,2)     node[ref]       (rr)    {\local{rebaseme}};

            \begin{scope}[thick,<-]
                \draw (c1) -- (c2);
            \draw (c2) -- (mstr);
                \draw (mstr) -- (head);

                \draw (c1) -- (cm);
                \draw (cm) -- (rm);

                \draw (c1) -- (cr);
                \draw (cr) -- (rr);
            \end{scope}
        \end{tikzpicture}
    \end{center}
    \shell{\scriptsize%
        \$ git merge mergeme \nvspnl
        Merge made by recursive. \nvspnl
         file7 |    2 +- \nvspnl
         file8 |    2 +- \nvspnl
         file9 |    2 +- \nvspnl
         3 files changed, 3 insertions(+), 3 deletions(-)
    }
}

\frame{%
    \frametitle{Rebasing -- visualized (after merge)}
    \begin{center}
        \begin{tikzpicture}[yscale=0.9]
            \tikzstyle{commit}=[rectangle, draw, rounded corners, fill=green!33]
            \tikzstyle{ref}=[rectangle, draw, rounded corners]

            \draw (0,1)     node[commit]    (cm)    {dcc1a52};
            \draw (0,2)     node[ref]       (rm)    {\local{mergeme}};

            \draw (2,0)     node[commit]    (c1)    {0e74e93};
            \draw (2,1)     node[commit]    (c2)    {89f8555};
            \draw (2,2)     node[commit]    (c3)    {f9ee4d3};
            \draw (2,3)     node[ref]       (mstr)  {\master};
            \draw (2,4)     node[ref]       (head)  {\head};

            \draw (4,1)     node[commit]    (cr)    {a5edb85};
            \draw (4,2)     node[ref]       (rr)    {\local{rebaseme}};

            \begin{scope}[thick,<-]
                \draw (c1) -- (c2);
                \draw (c2) -- (c3);
                \draw (c3) -- (mstr);
                \draw (mstr) -- (head);

                \draw (c1) -- (cm);
                \draw (cm) -- (c3);
                \draw (cm) -- (rm);

                \draw (c1) -- (cr);
                \draw (cr) -- (rr);
            \end{scope}
        \end{tikzpicture}
    \end{center}
    \shell{\scriptsize%
        \$ git checkout rebaseme \nvspnl
        Switched to branch `rebaseme' \nvspnl
        \$ git rebase master \nvspnl
        First, rewinding head to replay your work on top of it\dots \nvspnl
        Applying: Change contents of files 4 through 6.
    }
}

\frame{%
    \frametitle{Rebasing -- visualized (after rebase)}
    \begin{center}
        \begin{tikzpicture}[yscale=0.9]
            \tikzstyle{commit}=[rectangle, draw, rounded corners, fill=green!33]
            \tikzstyle{ref}=[rectangle, draw, rounded corners]

            \draw (0,1)     node[commit]    (cm)    {dcc1a52};
            \draw (0,2)     node[ref]       (rm)    {\local{mergeme}};

            \draw (2,0)     node[commit]    (c1)    {0e74e93};
            \draw (2,1)     node[commit]    (c2)    {89f8555};
            \draw (2,2)     node[commit]    (c3)    {f9ee4d3};
            \draw (2,3)     node[ref]       (mstr)  {\master};

            \draw (4,3)     node[commit]    (cr)    {0f2eb73};
            \draw (4,4)     node[ref]       (rr)    {\local{rebaseme}};
            \draw (4,5)     node[ref]       (head)  {\head};

            \begin{scope}[thick,<-]
                \draw (c1) -- (c2);
                \draw (c2) -- (c3);
                \draw (c3) -- (mstr);

                \draw (c1) -- (cm);
                \draw (cm) -- (c3);
                \draw (cm) -- (rm);

                \draw (c3) -- (cr);
                \draw (cr) -- (rr);
                \draw (rr) -- (head);
            \end{scope}
        \end{tikzpicture}
    \end{center}
}

\frame{%
    \frametitle{Rebasing -- onto \master}
    \begin{itemize}
        \item When you are done with your work on a topic branch, but the \master-branch was updated in the meantime, it is best (and in some projects required) to rebase your work on top of the updated branch.
            \shell{%
                \$ git fetch origin \newline
                \$ git rebase origin/master
            }
        \item It is generally a good idea to do this from time to time even if your work is not yet finished, because if merge conflicts occur, you can fix them while they are still easily manageable.
    \end{itemize}
}



\subsection{Pull/Merge requests}
\frame{%
    \frametitle{Pull/Merge requests}
    \begin{itemize}
        \item In most cases you will not be allowed to push to the main repository (in GitLab: the master branch) directly.
        \item To contribute to a project, you have to
        \begin{enumerate}
            \item clone the main repository,
            \item create a topic branch,
            \item do your work on it,
            \item if necessary rebase your work on the current branch head,
            \item \emph{test your changes (!)},
            \item push the changes to your own public repository (in GitLab: the main repository) and
            \item submit a so-called \emph{pull-request} (in GitLab: \emph{merge-request}) to let the maintainer know, that you have changes in your public repository, that you want her/him to merge.
        \end{enumerate}
    \end{itemize}
}



\subsection{Typical workflow}
\frame{%
    \frametitle{Typical workflow -- Clone or pull}
    \begin{enumerate}
        \item Clone or update the refs of the remote repository:
        \begin{itemize}
            \item Clone:
                \shell{\scriptsize%
                    \$ git clone <remote url> \nvspnl
                    \$ git branch <topic branch name> \nvspnl
                    \$ git checkout <topic branch name>
                }
            \item Pull:
                \shell{\scriptsize%
                    \$ git checkout master \nvspnl
                    \$ git pull origin master \nvspnl
                    \$ git branch <topic branch name> \nvspnl
                    \$ git checkout <topic branch name>
                }
        \end{itemize}
    \end{enumerate}
}

\frame{%
    \frametitle{Typical workflow -- Commit and push}
    \begin{enumerate}
        \setcounter{enumi}{1}
        \item Work on your topic branch:
            \shell{\scriptsize%
                [\dots some vimmin' \dots] \nvspnl
                \$ git diff \nvspnl
                \$ git stage <changed files> \nvspnl
                \$ git status \nvspnl
                \$ git commit
            }
            Repeat \dots
        \item Push:
            \shell{\scriptsize%
                \$ git fetch origin \nvspnl
                \$ git rebase origin/master \nvspnl
                [\dots resolve conflicts if necessary \dots] \nvspnl
                \$ git push origin <topic branch name>
            }
    \end{enumerate}
}
